
public class Xpath {

	// Basic xpath 
//	1. //input[@id='uid']
	
	// Text  : //a[text()='PERSONAL']
	// index :  (//a[text()='PERSONAL'])[2]
	// contains :  //h1[contains(text(),'Hello')]
	//starts-with : //h1[starts-with(text(),'Hello')]
	
	// and   :- //input[@id='uid' and @name='uid']
	// Or :- //input[@id='uid' or @name='uid']
	// /.. : //input[@id='uid']/..  (To reach on the immediate parent)
	
	// / : //form[@action='doLogin']/table  (Reach on immediate next node)
	
	// following : //form[@action='doLogin']//following::tbody
	
	// following-sibling : //td[contains(text(),'Username:')]/following-sibling::td/input[@id='uid']
	
	// preceding-sibling : //input[@id='uid']/../preceding-sibling::td
	
	// child : //form[@action='doLogin']//child::tbody
	// parent : //input[@id='uid']/parent::td
	// ansestor : //input[@id='uid']/ancestor::td
	
}
