package BaseClasses;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class ListnerTestNg implements ITestListener{

			
	    public void onFinish(ITestContext arg0) {					
	        System.out.println("finishing all the method");			
	        		
	    }		

	   	
	    public void onStart(ITestContext arg0) {					
	        System.out.println("starting all the methods");	
	        		
	    }		

	   	
	    public void onTestFailedButWithinSuccessPercentage(ITestResult res) {					
	       System.out.println("onTestFailedButWithinSuccessPercentage" + res.getName());			
	        		
	    }		

	   	
	    public void onTestFailure(ITestResult res) {					
	        System.out.println("onTestFailure " + res.getName());			
	        		
	    }		
	
	    public void onTestSkipped(ITestResult arg0) {					
	       System.out.println("onTestSkipped :"+ arg0.getName());				
	        		
	    }		

	   	
	    public void onTestStart(ITestResult arg0) {					
	        System.out.println( "on test start :" + arg0.getName());			
	        		
	    }		

	   		
	    public void onTestSuccess(ITestResult arg0) {					
	        System.out.println("on the success "+ arg0.getName());			
	        		
	    }		

}
