package BaseClasses;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import io.qameta.allure.Attachment;

public class AllureTestListner implements ITestListener{

	
	
	public static String getMethodName(ITestResult itName) {
		return itName.getMethod().getConstructorOrMethod().getName();
	}
	
	// Attachement for Allure
	@Attachment(value = "Page screenshot", type = "image/png")
	public byte[] saveScreenshot(WebDriver driver) {
	   return ((TakesScreenshot)driver ).getScreenshotAs(OutputType.BYTES);
		
	}
	
	@Attachment(value="{0}",type = "text/palin")
	public static String saveMsg(String msg) {
		return msg;
	}

    public void onFinish(ITestContext arg0) {					
        System.out.println("finishing all the method");			
        		
    }		

   	
    public void onStart(ITestContext arg0) {					
        System.out.println("starting all the methods");	
        		
    }		

   	
    public void onTestFailedButWithinSuccessPercentage(ITestResult res) {					
       System.out.println("onTestFailedButWithinSuccessPercentage" + res.getName());			
        		
    }		

   	
    public void onTestFailure(ITestResult res) {					
        System.out.println("onTestFailure " + res.getName());	
        
        WebDriver driver = BaseClass.getDriver();
        if(driver instanceof WebDriver) {
        	saveScreenshot(driver);
        }
        
        saveMsg(getMethodName(res) + "rounak Failed and screenshot");
        		
    }		

    public void onTestSkipped(ITestResult arg0) {					
       System.out.println("onTestSkipped :"+ arg0.getName());				
        		
    }		

   	
    public void onTestStart(ITestResult arg0) {					
        System.out.println( "on test start :" + arg0.getName());			
        		
    }		

   		
    public void onTestSuccess(ITestResult arg0) {					
        System.out.println("on the success "+ arg0.getName());			
        		
    }	
	
	
}
