package BaseClasses;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import UtilClasses.ActionUtilities;
import UtilClasses.BrowserBasics;

public class BaseClass {
	private static BaseClass baseClass;	
	public static Properties prop;	
	
	public static BrowserBasics BB;
	public static ActionUtilities Au;
	
	private  static ThreadLocal<WebDriver> drivers = new ThreadLocal<WebDriver>();

	
	public  BaseClass() {

		File file = new File(".//config.properties");
		try {
			FileInputStream fis = new FileInputStream(file);
			prop = new Properties();
			prop.load(fis);

		} catch (Exception e) {

			e.printStackTrace();
		}
	
	}
	protected void intialize() {	
		
		System.out.println("rounak Jain :- "+System.getProperty("browser"));
		
      WebDriver d =BrowserFactory.createBrowserInstance(prop.getProperty("browser"));
      drivers.set(d);
      getDriver().get(prop.getProperty("url"));
      initConfigParam();
	}
	
	public static  WebDriver getDriver() {
		
		return drivers.get();
	}


	public void initConfigParam() {
		BB= new BrowserBasics();
		Au= new ActionUtilities();
	}

	public void navigateURL(String str) {
		String url;
		if (str.equals("")) {
			url = prop.getProperty("url");
		} else {
			url = str;
		}
//		driver.get(url);
	}
}
