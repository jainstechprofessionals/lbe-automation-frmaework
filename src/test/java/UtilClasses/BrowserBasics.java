package UtilClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import BaseClasses.BaseClass;

public class BrowserBasics {

	public WebDriver returnDriver() {
		return BaseClass.getDriver();
	}

	public void sendKeys(By loc, String str) {
		returnDriver().findElement(loc).sendKeys(str);
	}
	
	public void click(By loc) {
		BaseClass.getDriver().findElement(loc).click();
	}

	public void selectByValue() {

	}

	public void selectByIndex() {

	}

	public void selectByVisibleText() {

	}

	public boolean isDisplay(WebElement we) {
		boolean flag;
		try {
			we.isDisplayed();
			flag = true;
		} catch (Exception e) {
//			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	public boolean isEnabled() {
		return false;

	}

	public boolean isSelected() {
		return false;

	}

	public void screenShots() {

	}

	public void frameHandle() {

	}

	public void moveToChildWIndow() {
	}

	public void moveToParentWindow() {

	}

}
