package UtilClasses;

import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import BaseClasses.BrowserBasicConstent;

public class ExcelUtility {

	private static XSSFWorkbook wb;
	private static XSSFSheet ws;
	private static XSSFRow row;
	private static XSSFCell cell;

	public static Object[][] getExcelData(String sheetName) {
		Object [][] arr = null;
		try {
		FileInputStream obj = new FileInputStream(BrowserBasicConstent.excelFilePath);

		wb = new XSSFWorkbook(obj);
		ws = wb.getSheet(sheetName);
		int totalRow = ws.getLastRowNum();
		System.out.println(totalRow); 
		
		int startRow=1;
		int startCol =0;
		int c =0;
		int d=0;
		arr= new Object[totalRow][2];
		for(int i=startRow;i<=totalRow;i++,c++) {
			d=0;
			for(int j=startCol;j<2;j++,d++) {
				arr[c][d]=getCellData(i,j);
				System.out.println(arr[c][d]);
			}
		}
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("in exe");
		}
      return arr;
	}

	public static Object getCellData(int rowNo, int colNum) {

		row = ws.getRow(rowNo);
		cell = row.getCell(colNum);

		CellType dataType = cell.getCellType();
		

		if (dataType.name().equalsIgnoreCase("string")) {
			return cell.getStringCellValue();
		} else {
			return cell.getStringCellValue();
		}
		

	}

}
