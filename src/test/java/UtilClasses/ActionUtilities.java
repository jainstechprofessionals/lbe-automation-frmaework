package UtilClasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import BaseClasses.BaseClass;

public class ActionUtilities {

	
	Actions act;
	
	
	public void mouseHover(WebElement we) {
		act = new Actions(BaseClass.getDriver());
		act.moveToElement(we).build().perform();
	}
	public void dragAndDrop(WebElement src, WebElement dest) {
		act = new Actions(BaseClass.getDriver());
		act.moveToElement(src).dragAndDrop(src, dest).build().perform();
		act.release();
	}
	
	
	
	
}
