package PageClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import BaseClasses.BaseClass;

public class HomePagePO  {
 

	By vas = By.xpath("View Account Summary");
	By g0 = By.id("btnGetAccount");
	
	public void clickGo() {
//		BaseClass.getDriver().findElement(g0).click();
		BaseClass.BB.click(g0);
	}
	
	public ViewAccountSummay clickViewAccountSummary() {
		BaseClass.getDriver().findElement(vas).click();
		return new ViewAccountSummay();
	}
	
	
}
