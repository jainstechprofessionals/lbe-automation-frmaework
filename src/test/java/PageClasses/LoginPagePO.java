package PageClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import BaseClasses.BaseClass;

public class LoginPagePO  {

	By uname = By.id("uid");
	By pwd = By.id("passw");
	By submitBtn = By.name("btnSubmit");
	

	public void enterUserName(String userName) {
//		BaseClass.getDriver().findElement(uname).sendKeys(userName);
		BaseClass.BB.sendKeys(uname, userName);
	}

	public void enterPassword(String password) {
//		getDriver().sendKeys(pwd, password);
		BaseClass.getDriver().findElement(pwd).sendKeys(password);
	}

	public void clickSubmit() {
		BaseClass.getDriver().findElement(submitBtn).click();
	}

	public String gettitle() {
		return BaseClass.getDriver().getTitle();
	}

	public HomePagePO login(String userName, String password) {
		enterUserName(userName);
		enterPassword(password);
		clickSubmit();
		return new HomePagePO();
	}

}
