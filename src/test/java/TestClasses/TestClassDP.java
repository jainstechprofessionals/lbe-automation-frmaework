package TestClasses;

import org.testng.annotations.Test;

import UtilClasses.ExcelUtility;

import java.lang.reflect.Method;

import org.testng.ITestContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

public class TestClassDP {
 
	
	@BeforeMethod
	public void bm() {
		System.out.println("in before method");
	}
	
	
	
	@Test(dataProvider = "dp",dataProviderClass = TestClassDP.class)
  public void validLogin(String n, String s) {
	  
	  System.out.println(n);
	  System.out.println(s);
	  
	  
  }

	
	@Test(dataProvider = "dp",groups = "a")
	  public void inLogin(Integer n, String s) {
		  
		  System.out.println(n);
		  System.out.println(s);
		  
		  
	  }
  @DataProvider
  public Object[][] dp(Method m) {
  
//	  String s= m.getName();
//	  System.out.println(s);
//	  if(s.contains("valid")) {
//		  return new Object[][] {
//			     { 1, "a" },
//			     { 2, "b" },
//			    };
//	  }else {
//		  return new Object[][] {
//			     { 1, "rounak" },
//			     { 2, "jain" },
//			    };
//	  }
	  
	  return ExcelUtility.getExcelData("Sheet1");
	  
	  
   
  }
}
