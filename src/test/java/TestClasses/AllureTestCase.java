package TestClasses;

import java.io.File;



import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import BaseClasses.AllureTestListner;
import BaseClasses.BaseClass;

import PageClasses.LoginPagePO;
import io.qameta.allure.AllureId;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Flaky;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;


//@Listeners(AllureTestListner.class)
public class AllureTestCase extends BaseClass {
	LoginPagePO lp;
//	Logger log = Logger.getLogger(LoginTest.class);	
	

	@BeforeMethod
	public void intialize1() {
		intialize();
		lp = new LoginPagePO();

	}

	@Test(description = "This test case is to verify the login function")
	@Description("RounakAllure : This test case is to verify the login function")	
	@Epic("Epic US_2345")
	@Feature("Login Feature")
	@Story("Story IND_345")
	@AllureId("rounak 123")
	@Severity(SeverityLevel.BLOCKER)
	
	public void verifyLogin() throws InterruptedException {

		String str = lp.gettitle();
		
		System.out.println(str);
//		Thread.sleep(4000);
		Assert.assertEquals(str, "Altoro Mutual");
	

	}
	
	@Test (description = "this is to verify with wrong info")
	@Feature("Login wrong Feature")
	@Story("Story IND_000")
	@AllureId("rounak 545")
	@Severity(SeverityLevel.BLOCKER)
	@Description("RounakAllure : This test case is to verify the login function")	
	@Epic("Epic US_0989")
	@Flaky
	
	public void verifyLogin1() throws InterruptedException {


		lp.login("jsmith", "demo1234");
		String str = lp.gettitle();
		System.out.println(str);
		Thread.sleep(4000);
		Assert.assertEquals(str, "Altoro Mutual");

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		getDriver().quit();
		
	}
}
