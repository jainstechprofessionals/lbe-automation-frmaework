package TestClasses;

import java.io.File;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import BaseClasses.BaseClass;

import PageClasses.LoginPagePO;

public class LoginTest extends BaseClass {
	LoginPagePO lp;
//	Logger log = Logger.getLogger(LoginTest.class);	
	Logger log = Logger.getLogger("devpinoyLogger");

	ExtentReports extent = new ExtentReports();;
	ExtentSparkReporter spark = new ExtentSparkReporter(".//automation.html");

	@BeforeMethod
	public void intialize1() {
		intialize();
		lp = new LoginPagePO();

	}

	@Test
	public void verifyLogin() throws InterruptedException {

		spark.config().setTheme(Theme.DARK);
		spark.config().setDocumentTitle("Jain_Rounak");
		spark.getConf().setReportName("smita");

		extent.attachReporter(spark);

		ExtentTest test = extent.createTest("verifyLogin").assignCategory("smoke").assignDevice("Windows");

		log.info("-----start------------");
		log.debug("Start Debugging");
		lp.login("jsmith", "demo1234");
		log.error("Not able to login");
		String str = lp.gettitle();
		test.pass(str);
		System.out.println(str);
//		Thread.sleep(4000);
		Assert.assertEquals(str, "Altoro Mutual");
		test.pass("Test case passed");

	}

	@Test
	public void verifyLogin1() throws InterruptedException {

		spark.config().setTheme(Theme.DARK);
		spark.config().setDocumentTitle("Jain_Rounak");
		spark.getConf().setReportName("smita");

		extent.attachReporter(spark);
		ExtentTest test = extent.createTest("verifyLogin  failed adsfdasfdfsa");
		test.fail("failded due to ");
		test.createNode("node1").pass("asdfasfasfasdfsdfa");
		lp.login("jsmith", "demo1234");
		String str = lp.gettitle();
		System.out.println(str);
		Thread.sleep(4000);
		Assert.assertEquals(str, "Altoro Mutual1");

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		getDriver().quit();
		extent.flush();
	}
}
